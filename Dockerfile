FROM maven

LABEL maintainer="Yoshitake Kitamura <kitamura@epea.co.jp>"
RUN apt-get update && apt-get install -y \
    openssh-client \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -u 1000 -m yoshitake

